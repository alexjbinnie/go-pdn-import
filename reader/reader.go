package reader

import (
	"encoding/binary"
	"os"
	. "pdn-import/errors"
)

type File struct {
	file *os.File
}

func (file *File) Close() {
	file.file.Close()
}

func (file *File) ReadBytes(chars int) ([] byte, error) {
	b1 := make([]byte, chars)
	n, err := file.file.Read(b1)
	CatchError(err)
	if n < chars {
		return b1, NewError("Insufficient bytes to read in data")
	}
	return b1, nil
}

func (file *File) ReadAsciiString(chars int) (string, error) {
	b1, err := file.ReadBytes(chars)
	return string(b1), err
}

func (file *File) ReadUtf8String(chars int) (string, error) {
	b1, err := file.ReadBytes(chars)
	return string(b1), err
}

func (file *File) ReadByte() (byte, error) {
	b1, err := file.ReadBytes(1)
	return b1[0], err
}

func (file *File) ReadInt32LittleEndian() (int32, error) {
	b1, err := file.ReadBytes(4)
	return int32(binary.LittleEndian.Uint32(b1)), err
}

func (file *File) ReadUInt32LittleEndian() (uint32, error) {
	b1, err := file.ReadBytes(4)
	return binary.LittleEndian.Uint32(b1), err
}

func (file *File) ReadUInt32BigEndian() (uint32, error) {
	b1, err := file.ReadBytes(4)
	return binary.BigEndian.Uint32(b1), err
}

func ReadFile(path string) File {
	f, err := os.Open(path)
	CatchError(err)
	return File{f}
}
