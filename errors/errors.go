package errors

import (
	"errors"
	"fmt"
	"strings"
)

func CatchError(e error) {
	if e != nil {
		panic(e)
	}
}

func Panic(error string, args ...interface{}) {
	panic(NewError(error, args))
}

var logLevel int = 0

func IncreaseLogIndent() {
	logLevel++
}

func DecreaseLogIndent() {
	logLevel--
}

func Log(error string, args ...interface{}) {
	fmt.Print(strings.Repeat("\t", logLevel) )
	string := fmt.Sprintf(error, args...)
	fmt.Println(string)
}


func NewError(error string, args ...interface{}) error {
	return errors.New(fmt.Sprintf(error, args))
}
