package nrbf

import (
	"encoding/binary"
	"fmt"
	"math"
	. "pdn-import/errors"
	. "pdn-import/reader"
	"strconv"
	"time"
	"unicode/utf8"
)

type RecordType int
type BinaryType int
type PrimitiveType int
type BinaryArrayType int
type ClassId int
type ObjectId int
type LibraryId int

type ObjectNullMultiple struct {
	count int
}

type MessageEnd struct {
}

type BinaryLibrary struct {
	_id     LibraryId
	name    string
	classes map[ClassId]*Class
}

type Reference struct {
	_id                ObjectId
	parent             interface{}
	indexInParent      int
	resolved           bool
	collectionResolver interface{}
	originalObj        interface{}
}

const (
	RecordTypeSerializedStreamHeader         RecordType = 0
	RecordTypeClassWithId                    RecordType = 1
	RecordTypeSystemClassWithMembers         RecordType = 2
	RecordTypeClassWithMembers               RecordType = 3
	RecordTypeSystemClassWithMembersAndTypes RecordType = 4
	RecordTypeClassWithMembersAndTypes       RecordType = 5
	RecordTypeBinaryObjectString             RecordType = 6
	RecordTypeBinaryArray                    RecordType = 7
	RecordTypeMemberPrimitiveTyped           RecordType = 8
	RecordTypeMemberReference                RecordType = 9
	RecordTypeObjectNull                     RecordType = 10
	RecordTypeMessageEnd                     RecordType = 11
	RecordTypeBinaryLibraryRecord            RecordType = 12
	RecordTypeObjectNullMultiple256          RecordType = 13
	RecordTypeObjectNullMultiple             RecordType = 14
	RecordTypeArraySinglePrimitive           RecordType = 15
	RecordTypeArraySingleObject              RecordType = 16
	RecordTypeArraySingleString              RecordType = 17
	RecordTypeMethodCall                     RecordType = 21
	RecordTypeMethodReturn                   RecordType = 22
)

const (
	BinaryTypePrimitive      BinaryType = 0
	BinaryTypeString         BinaryType = 1
	BinaryTypeObject         BinaryType = 2
	BinaryTypeSystemClass    BinaryType = 3
	BinaryTypeClass          BinaryType = 4
	BinaryTypeObjectArray    BinaryType = 5
	BinaryTypeStringArray    BinaryType = 6
	BinaryTypePrimitiveArray BinaryType = 7
)

func (typeInfo *MemberTypeInfo) String() string {
	switch typeInfo.binaryType {
	case BinaryTypePrimitive:
		primitiveType := typeInfo.additionalInfo.(PrimitiveType)
		return primitiveType.String()
	case BinaryTypeString:
		return "String"
	case BinaryTypeObject:
		return "Object"
	case BinaryTypeSystemClass:
		return "System Class {" + typeInfo.additionalInfo.(string) + "}"
	case BinaryTypeClass:
		return "Class"
	case BinaryTypeObjectArray:
		return "Object Array"
	case BinaryTypeStringArray:
		return "String Array"
	case BinaryTypePrimitiveArray:
		primitiveType := typeInfo.additionalInfo.(PrimitiveType)
		return primitiveType.String() + "[]"
	}
	return "ERROR"
}

const (
	PrimitiveTypeBoolean  PrimitiveType = 1
	PrimitiveTypeByte     PrimitiveType = 2
	PrimitiveTypeChar     PrimitiveType = 3
	PrimitiveTypeDecimal  PrimitiveType = 5
	PrimitiveTypeDouble   PrimitiveType = 6
	PrimitiveTypeInt16    PrimitiveType = 7
	PrimitiveTypeInt32    PrimitiveType = 8
	PrimitiveTypeInt64    PrimitiveType = 9
	PrimitiveTypeSByte    PrimitiveType = 10
	PrimitiveTypeSingle   PrimitiveType = 11
	PrimitiveTypeTimeSpan PrimitiveType = 12
	PrimitiveTypeDateTime PrimitiveType = 13
	PrimitiveTypeUInt16   PrimitiveType = 14
	PrimitiveTypeUInt32   PrimitiveType = 15
	PrimitiveTypeUInt64   PrimitiveType = 16
	PrimitiveTypeNull     PrimitiveType = 17
	PrimitiveTypeString   PrimitiveType = 18
)

func (primitive *PrimitiveType) String() string {
	switch *primitive {
	case PrimitiveTypeBoolean:
		return "bool"
	case PrimitiveTypeByte:
		return "byte"
	case PrimitiveTypeChar:
		return "char"
	case PrimitiveTypeDecimal:
		return "decimal"
	case PrimitiveTypeDouble:
		return "double"
	case PrimitiveTypeInt16:
		return "int16"
	case PrimitiveTypeInt32:
		return "int32"
	case PrimitiveTypeInt64:
		return "int64"
	case PrimitiveTypeSByte:
		return "sbyte"
	case PrimitiveTypeSingle:
		return "single"
	case PrimitiveTypeTimeSpan:
		return "TimeSpan"
	case PrimitiveTypeDateTime:
		return "DateTime"
	case PrimitiveTypeUInt16:
		return "uint16"
	case PrimitiveTypeUInt32:
		return "uint32"
	case PrimitiveTypeUInt64:
		return "uint64"
	case PrimitiveTypeNull:
		return "null"
	case PrimitiveTypeString:
		return "string"
	}
	return "ERROR"
}

const (
	BinaryArrayTypeSingle            BinaryArrayType = 0
	BinaryArrayTypeJagged            BinaryArrayType = 1
	BinaryArrayTypeRectangular       BinaryArrayType = 2
	BinaryArrayTypeSingleOffset      BinaryArrayType = 3
	BinaryArrayTypeJaggedOffset      BinaryArrayType = 4
	BinaryArrayTypeRectangularOffset BinaryArrayType = 5
)

type NRFB struct {
	file            *File
	rootID          ObjectId
	headerID        int32
	binaryLibraries map[LibraryId]BinaryLibrary
	classByID       map[ObjectId]*Class
	objectsByID     map[ObjectId]interface{}
}

func (self *NRFB) readByte() byte {
	result, err := self.file.ReadByte()
	CatchError(err)
	return result
}

func (self *NRFB) readLengthPrefixedString() string {
	length := 0

	// Each bit range is 7 bits long with a maximum of 5 bytes
	for bit_range := 0; ; bit_range += 7 {
		if bit_range >= 5*7 {
			Panic("NRBF LengthPrefixedString overflow")
		}

		bytee := self.readByte()

		// Remove the last bit from the length (used to indicate if there is another byte to come)
		// Then shift the number to the appropriate bit range and add it
		length += int(bytee&((1<<7)-1)) << bit_range

		// Check MSB and if it is zero, this is the last length byte and we are ready to read string
		if bytee&(1<<7) == 0 {
			break
		}
	}

	// Read the string
	str, err := self.file.ReadUtf8String(length)
	CatchError(err)
	return str
}

func (self *NRFB) readSerializationHeaderRecord() {
	self.rootID = ObjectId(self.readInt32())
	self.headerID = self.readInt32()
	majorVersion, minorVersion := self.readInt32(), self.readInt32()

	if majorVersion != 1 || minorVersion != 0 {
		Panic("Major and minor version for serialization header is incorrect: %d %d", majorVersion, minorVersion)
	}

	if self.rootID == 0 {
		Panic("Root ID is zero indicating that a BinaryMethodCall is available. Not implemented yet")
	}

	Log("Read Serialization Header")
	IncreaseLogIndent()
	defer DecreaseLogIndent()
	Log("Serialization Version %d.%d", majorVersion, minorVersion)
	Log("Root ID is %d", self.rootID)
}

func ReadNRBF(file *File) NRFB {
	nrbf := NRFB{}
	nrbf.file = file
	nrbf.binaryLibraries = make(map[LibraryId]BinaryLibrary)
	nrbf.classByID = make(map[ObjectId]*Class)
	nrbf.objectsByID = make(map[ObjectId]interface{})
	nrbf.read()
	return nrbf
}

func (self *NRFB) read() {
	self.readRecord()
	if self.rootID == 0 {
		Panic("Invalid stream, unable to read header. File may be corrupted")
	}

	readRecordAndCheckIfEnd := func() bool {
		_, ok := self.readRecord().(MessageEnd)
		return !ok
	}

	for readRecordAndCheckIfEnd() {

	}

	self.resolveReferences()
}

func (self *NRFB) resolveReferences() {

	// Loop through all of the objects
	for _, object := range self.objectsByID {
		// Attempt to iterate through the object
		// If it doesnt work, then move on to the next item because there wont need to be any
		// references.
		// Otherwise, if any of the items have an object ID, create a reference
		if cls, ok := object.(Object); ok {
			for name, member := range cls.members {
				if ref, ok := member.(Reference); ok {
					cls.members[name] = self.objectsByID[ref._id]
				}
			}
		}
		if cls, ok := object.([]interface{}); ok {
			for id, member := range cls {
				if ref, ok := member.(Reference); ok {
					cls[id] = self.objectsByID[ref._id]
				}
			}
		}
	}
}

func (self *NRFB) readBinaryLibrary() *BinaryLibrary {
	libraryID := LibraryId(self.readInt32())
	libraryName := self.readLengthPrefixedString()
	library := BinaryLibrary{libraryID, libraryName, make(map[ClassId]*Class, 0)}
	self.binaryLibraries[libraryID] = library

	Log("Create Binary Library called '%s' with ID %d", libraryName, libraryID)

	return &library
}



func (self *NRFB) readClassInfo(isSystemClass bool) (*Class, *Object) {
	objectID := ObjectId(self.readInt32())
	classID := ClassId(objectID)
	className := self.readLengthPrefixedString()
	memberCount := self.readInt32()

	memberNames := make([]string, 0)

	for i := 0; i < int(memberCount); i++ {
		memberNames = append(memberNames, self.readLengthPrefixedString())
	}

	Log("Create Class '%s' with ID %d with %v members", className, classID, memberNames)

	cls := Class{classID: classID,
		isSystemClass: isSystemClass,
		className:     className,
		memberNames:   memberNames,
		}

	Log("Create Object of type '%s' with ID %d", className, objectID)

	obj := Object{objectID: objectID,
		members:       make(map[string]interface{}, 0)}

	// Class ID is a special identifier to represent the class itself but not the data within of it
	// For instance, you can declare the class once but then instantiate it multiple times
	// The object ID is the unique identifier for the object itself and cannot be repeated
	// In this instance, the classID and objectID are the same for the first instantiation
	// The only way to get these different is to have a ClassWithID record and then this is
	// set manually
	self.classByID[objectID] = &cls

	return &cls, &obj
}

type MemberTypeInfo struct {
	binaryType     BinaryType
	additionalInfo interface{}
}

func (self *NRFB) readAdditionalInfo(binaryType BinaryType) interface{} {

	switch binaryType {
	case BinaryTypePrimitive:
		return PrimitiveType(self.readByte())
	case BinaryTypePrimitiveArray:
		return PrimitiveType(self.readByte())
	case BinaryTypeSystemClass:
		return self.readLengthPrefixedString()
	case BinaryTypeClass:
		// The ClassTypeInfo structure is read and ignored
		self.readLengthPrefixedString()
		self.readInt32()
	}

	return nil
}

func (self *NRFB) readMemberTypeInfo(cls *Class) {
	binaryTypes := make([]BinaryType, 0)
	for range cls.memberNames {
		binaryTypes = append(binaryTypes, BinaryType(self.readByte()))
	}

	additionalInfo := make([]interface{}, 0)
	for _, binaryType := range binaryTypes {
		additionalInfo = append(additionalInfo, self.readAdditionalInfo(binaryType))
	}

	typeInfo := make([]MemberTypeInfo, 0)
	for i := range cls.memberNames {
		tInfo := MemberTypeInfo{binaryType: binaryTypes[i], additionalInfo: additionalInfo[i]}
		typeInfo = append(typeInfo, tInfo)
	}

	cls.typeInfo = typeInfo
}

func (self *NRFB) readPrimitive(primitiveType PrimitiveType) interface{} {
	switch primitiveType {
	case PrimitiveTypeBoolean:
		return self.readBool()
	case PrimitiveTypeByte:
		return self.readByte()
	case PrimitiveTypeChar:
		return self.readChar()
	case PrimitiveTypeDecimal:
		return self.readDecimal()
	case PrimitiveTypeDouble:
		return self.readDouble()
	case PrimitiveTypeInt16:
		return self.readInt16()
	case PrimitiveTypeInt32:
		return self.readInt32()
	case PrimitiveTypeInt64:
		return self.readInt64()
	case PrimitiveTypeSByte:
		return self.readSByte()
	case PrimitiveTypeSingle:
		return self.readSingle()
	case PrimitiveTypeTimeSpan:
		return self.readTimeSpan()
	case PrimitiveTypeDateTime:
		return self.readDateTime()
	case PrimitiveTypeUInt16:
		return self.readUInt16()
	case PrimitiveTypeUInt32:
		return self.readUInt32()
	case PrimitiveTypeUInt64:
		return self.readUInt64()
	case PrimitiveTypeNull:
		return nil
	case PrimitiveTypeString:
		return self.readLengthPrefixedString()
	default:
		Panic("Failed to handle primitive type ", primitiveType)
	}
	return nil
}

func (self *NRFB) readBool() bool {
	return self.readByte() > 0
}

func (self *NRFB) readChar() rune {
	utf8Bytes := make([]byte, 0)
	for {
		utf8Bytes = append(utf8Bytes, self.readByte())
		rune, _ := utf8.DecodeRune(utf8Bytes)
		if rune != utf8.RuneError {
			return rune
		}
		if len(utf8Bytes) > 4 {
			Panic("Invalid char read from NRBF file, longer than 4 bytes")
		}
	}
}

func (self *NRFB) readDecimal() float64 {
	str := self.readLengthPrefixedString()
	i, err := strconv.ParseFloat(str, 64)
	CatchError(err)
	return i
}

func (self *NRFB) readDouble() float64 {
	bits := self.readUInt64()
	return math.Float64frombits(bits)
}

func (self *NRFB) readSingle() float32 {
	bits := self.readUInt32()
	return math.Float32frombits(bits)
}

func (self *NRFB) readUInt16() uint16 {
	bytes, err := self.file.ReadBytes(2)
	CatchError(err)
	return binary.LittleEndian.Uint16(bytes)
}

func (self *NRFB) readUInt32() uint32 {
	bytes, err := self.file.ReadBytes(4)
	CatchError(err)
	return binary.LittleEndian.Uint32(bytes)
}

func (self *NRFB) readUInt64() uint64 {
	bytes, err := self.file.ReadBytes(8)
	CatchError(err)
	return binary.LittleEndian.Uint64(bytes)
}

func (self *NRFB) readInt16() int16 {
	return int16(self.readUInt16())
}

func (self *NRFB) readInt32() int32 {
	return int32(self.readUInt32())
}

func (self *NRFB) readInt64() int64 {
	return int64(self.readUInt64())
}

func (self *NRFB) readSByte() int8 {
	return int8(self.readByte())
}

func (self *NRFB) readTimeSpan() time.Duration {
	// 64-bit integer that represents time span in increments of 100 nanoseconds
	return time.Duration(self.readInt64() * 100)
}

func (self *NRFB) readDateTime() interface{} {
	Panic("UNIMPLEMENTED - DateTime parsing")
	return nil
}

func (self *NRFB) readClassMembers(obj *Object, cls *Class) *Object {

	for index, memberName := range cls.memberNames {
		// If typeinfo is not defined, as is the case for ClassWithMembers and SystemClassWithMembers,
		// then assume it is an object that can be read
		// Not sure if this is a safe assumption because Microsoft isn't entirely clear when the member type
		// information is 'unnecessary'

		var binaryType BinaryType
		var additionalInfo interface{}

		if cls.typeInfo != nil {
			typeInfo := cls.typeInfo[index]
			binaryType = typeInfo.binaryType
			additionalInfo = typeInfo.additionalInfo
		} else {
			binaryType = BinaryTypeObject
			additionalInfo = nil
		}

		typeInfo := MemberTypeInfo{binaryType: binaryType, additionalInfo: additionalInfo}

		Log("Reading Member %s of type %s", memberName, typeInfo.String())
		IncreaseLogIndent()

		var value interface{}

		if binaryType == BinaryTypePrimitive {
			value = self.readPrimitive(additionalInfo.(PrimitiveType))
			Log("Read primitive value %+v", value)
		} else {
			value = self.readRecord()

			if _, ok := value.(BinaryLibrary); ok {
				// BinaryLibrary can precede the actual member
				// Continue on to the actual member
				continue
			} else if nullMultiple, ok := value.(ObjectNullMultiple); ok {
				// Skip a given number of indices
				index += int(nullMultiple.count)
				Log("Read %d null objects", nullMultiple.count)
				continue
			} else if ref, ok := value.(Reference); ok {
				ref.parent = obj
				ref.indexInParent = index
				value = ref
				Log("Read reference to object with ID %d", ref._id)
			}
		}

		obj.members[memberName] = value

		DecreaseLogIndent()
	}

	// If this object is a .NET collection (e.g. a Generic dict or list) which can be
	// replaced by a native Python type, insert a collection _Reference instead of the raw
	// object which will be resolved later in read() using a "collection resolver"
	if cls.isSystemClass {
		/*
			for name, resolver in self._collectionResolvers:
			if obj.__class__.__name__.startswith('System_Collections_Generic_%s_' % name):
			obj = Reference(objectID, collectionResolver=resolver, originalObj=obj)
			self._collectionReferences.append(obj)
			break
		*/
	}

	self.objectsByID[obj.objectID] = *obj

	return obj
}

func (self *NRFB) readSystemClassWithMembersAndTypes() *Class {
	cls, obj := self.readClassInfo(true)

	IncreaseLogIndent()
	defer DecreaseLogIndent()

	self.readMemberTypeInfo(cls)
	self.readClassMembers(obj, cls)
	return cls
}

func (self *NRFB) readClassWithMembersAndTypes() *Class {
	cls, obj := self.readClassInfo(false)

	IncreaseLogIndent()
	defer DecreaseLogIndent()

	self.readMemberTypeInfo(cls)

	libraryID := LibraryId(self.readInt32())

	// Use libraryID to append the class to that binary library
	// This is particularly useful when saving the items again so that you save all of a binary library at once
	if libraryID >= 0 {
		self.binaryLibraries[libraryID].classes[cls.classID] = cls
	}

	self.readClassMembers(obj, cls)

	return cls
}

func (self *NRFB) readMemberReference() Reference {
	// objectID
	ref := Reference{_id: ObjectId(self.readInt32())}

	return ref
}

func (self *NRFB) readMessageEnd() MessageEnd {
	return MessageEnd{}
}

func (self *NRFB) readObjectNullMultiple256() ObjectNullMultiple {
	return ObjectNullMultiple{count: int(self.readByte())}
}

func (self *NRFB) readObjectNullMultiple() ObjectNullMultiple {
	return ObjectNullMultiple{count: int(self.readInt32())}
}

func (self *NRFB) readObjectArray(length int, objectId ObjectId) []interface{} {
	array := make([]interface{}, length)

	for index := 0; index < length; index++ {
		Log("Reading element %d", index)
		IncreaseLogIndent()

		value := self.readRecord()

		if _, ok := value.(BinaryLibrary); ok {
			// BinaryLibrary can precede the actual member
			// Continue on to the actual member
			DecreaseLogIndent()
			continue
		} else if nullMultiple, ok := value.(ObjectNullMultiple); ok {
			// Skip a given number of indices
			index += nullMultiple.count
			Log("Read %d null objects", nullMultiple.count)
			DecreaseLogIndent()
			continue
		} else if ref, ok := value.(Reference); ok {
			ref.parent = array
			ref.indexInParent = index
			value = ref
			Log("Read reference to object with ID %d", ref._id)
		}

		array[index] = value
		DecreaseLogIndent()

	}

	return array
}

func (self *NRFB) readBinaryArray() []interface{} {

	objectID := ObjectId(self.readInt32())
	arrayType := BinaryArrayType(self.readByte())
	rank := self.readInt32()
	lengths := make([]int32, rank)
	for i := range lengths {
		lengths[i] = self.readInt32()
	}



	// The lower bounds are ignored currently
	// Not sure of the implications or purpose of this
	if arrayType == BinaryArrayTypeSingleOffset || arrayType == BinaryArrayTypeJaggedOffset || arrayType == BinaryArrayTypeRectangularOffset {
		//lowerBounds = [self._readInt32() for i in range (rank)]
	}

	binaryType := BinaryType(self.readByte())
	additionalInfo := self.readAdditionalInfo(binaryType)

	typeInfo := MemberTypeInfo{binaryType: binaryType, additionalInfo: additionalInfo}

	Log("Create Binary Array with ID %d of type %s", objectID, typeInfo.String())
	IncreaseLogIndent()
	defer DecreaseLogIndent()

	// Get total length of items that we need to read
	// This is just the product of all the elements in the lengths array
	length := 1
	for _, l := range lengths {
		length *= int(l)
	}

	var array []interface{}

	// If the items are primitives, use primitive array readers
	// Otherwise, the items will be objects and should be read by reading records
	if binaryType == BinaryTypePrimitive {
		Log("Reading primitive array of length %d", length)
		array = make([]interface{}, 0)
		for i := 0; i < length; i++ {
			array[i] = self.readPrimitive(additionalInfo.(PrimitiveType))
		}
	} else {
		Log("Reading object array of length %d", length)
		array = self.readObjectArray(length, objectID)
	}

	// For a multidimensional array, take the 1D array that was read and convert it to ND
	if arrayType == BinaryArrayTypeRectangular || arrayType == BinaryArrayTypeRectangularOffset {
		Panic("Unimplemented")
		//array = convert1DArrayND(array, lengths)
	}

	// Save the object by ID
	// Only required for primitive because _readObjectArray saves the ID for you
	// But we just overwrite it regardless
	self.objectsByID[objectID] = array

	return array
}

func (self *NRFB) readBinaryObjectString() string {
	objectID := ObjectId(self.readInt32())
	str := self.readLengthPrefixedString()
	self.objectsByID[objectID] = str

	Log("Read string '%s' with ObjectID %d", str, objectID)

	return str
}

func (self *NRFB) readClassWithId() *Object {
	objectID := ObjectId(self.readInt32())
	metadataID := ObjectId(self.readInt32())
	cls := self.classByID[metadataID]

	Log("Create Object of type '%s' with ID %d", cls.className, objectID)

	obj := Object{
		objectID: objectID,
		class: cls,
		members: make(map[string]interface{}, 0),
	}

	IncreaseLogIndent()
	defer DecreaseLogIndent()

	// Only instance where the objectID does NOT equal the class ID is here!
	return self.readClassMembers(&obj, cls)
}

func (self *NRFB) readArraySingleObject() []interface{} {
	objectID := ObjectId(self.readInt32())
	length := int(self.readInt32())

	Log("Reading object array of length %d with ObjectID %d", length, objectID)
	IncreaseLogIndent()


	array := self.readObjectArray(length, objectID)
	self.objectsByID[objectID] = array

	DecreaseLogIndent()

	return array
}

func (nrfb *NRFB) GetRoot() interface{} {
	return nrfb.objectsByID[nrfb.rootID]
}

func (nrfb *NRFB) readRecord() interface{} {
	recordType := RecordType(nrfb.readByte())

	switch recordType {
	case RecordTypeSerializedStreamHeader:
		nrfb.readSerializationHeaderRecord()
		return nil
	case RecordTypeClassWithMembersAndTypes:
		return nrfb.readClassWithMembersAndTypes()
	case RecordTypeMemberReference:
		return nrfb.readMemberReference()
	case RecordTypeMessageEnd:
		return nrfb.readMessageEnd()
	case RecordTypeBinaryLibraryRecord:
		return nrfb.readBinaryLibrary()
	case RecordTypeObjectNullMultiple:
		return nrfb.readObjectNullMultiple()
	case RecordTypeObjectNullMultiple256:
		return nrfb.readObjectNullMultiple256()
	case RecordTypeSystemClassWithMembersAndTypes:
		return nrfb.readSystemClassWithMembersAndTypes()
	case RecordTypeBinaryArray:
		return nrfb.readBinaryArray()
	case RecordTypeBinaryObjectString:
		return nrfb.readBinaryObjectString()
	case RecordTypeClassWithId:
		return nrfb.readClassWithId()
	case RecordTypeArraySingleObject:
		return nrfb.readArraySingleObject()
	case RecordTypeArraySingleString:
		return nrfb.readArraySingleObject()
	case RecordTypeObjectNull:
		return nil
	}

	fmt.Print("\n")

	Panic("Unhandled record type %i", recordType)
	return nil
}
