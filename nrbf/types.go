package nrbf

type Class struct {
	classID       ClassId
	isSystemClass bool
	className     string
	memberNames   []string
	typeInfo      []MemberTypeInfo
}

type Object struct {
	objectID ObjectId
	class    *Class
	members  map[string]interface{}
}

func (obj *Object) GetMembers() []string {
	return obj.class.memberNames
}

func (obj *Object) GetMember(name string) interface{} {
	return obj.members[name]
}
