package pdnimport

import (
	"bytes"
	"compress/gzip"
	"encoding/binary"
	"fmt"
	"image"
	"io/ioutil"
	"math"
	. "pdn-import/errors"
	. "pdn-import/nrbf"
	. "pdn-import/reader"
)

type PdnImage struct {
	Width  int
	Height int
	Layers map[string]image.Image
}

func (img *PdnImage) Bounds() image.Rectangle {
	return image.Rect(0, 0, img.Width, img.Height)
}

func (img *PdnImage) GetLayer(name string) image.Image {
	return img.Layers[name]
}

func (img *PdnImage) GetLayers() []string {
	keys := make([]string, len(img.Layers))
	i := 0
	for k := range img.Layers {
		keys[i] = k
		i++
	}
	return keys
}

type LayeredImage interface {
	Bounds() image.Rectangle
	GetLayer(name string) image.Image
	GetLayers() []string
}

func LoadPdn(filename string) LayeredImage {
	file := ReadFile(filename)

	defer file.Close()

	// Begin by reading magic str and checking it
	magicStr, err := file.ReadAsciiString(4)
	CatchError(err)

	if magicStr != "PDN3" {
		Panic("Invalid magic string for PDN file: %s", magicStr)
	}

	headerSizeStr, err := file.ReadBytes(3)
	if err != nil {
		Panic("Unable to read header size. File may be corrupted.")
	}
	headerSizeStr = append(headerSizeStr, 00)

	headerSize := binary.LittleEndian.Uint32(headerSizeStr)
	println("Header Size", headerSize)

	// Header
	file.ReadBytes(int(headerSize))

	dataIndicator, err := file.ReadBytes(2)
	if err != nil || !bytes.Equal(dataIndicator, []byte{0, 1}) {
		Panic("Invalid or missing data indicator bytes. File may be corrupted")
	}

	nrbfData := ReadNRBF(&file)

	pdnDocument := nrbfData.GetRoot().(Object)

	width := pdnDocument.GetMember("width").(int32)
	height := pdnDocument.GetMember("height").(int32)

	layeredImage := PdnImage{Width: int(width),
		Height: int(height),
		Layers: make(map[string]image.Image, 0)}

	layers := pdnDocument.GetMember("layers").(Object)

	size := int(layers.GetMember("ArrayList+_size").(int32))
	layerArray := layers.GetMember("ArrayList+_items").([]interface{})

	for i := 0; i < size; i++ {
		layer := layerArray[i].(Object)

		layerProperties := layer.GetMember("Layer+properties").(Object)
		layerWidth := layer.GetMember("Layer+width").(int32)
		layerHeight := layer.GetMember("Layer+height").(int32)

		layerName := layerProperties.GetMember("name").(string)

		fmt.Println(i, layerName)

		if width != layerWidth || height != layerHeight {
			Panic("Layer is wrong size")
		}

		surface := layer.GetMember("surface").(Object)

		stride := surface.GetMember("stride").(int32)
		scan0 := surface.GetMember("scan0").(Object)
		length := uint32(scan0.GetMember("length64").(int64))

		data := make([]byte, length)

		formatVersion, _ := file.ReadByte()
		chunkSize, _ := file.ReadUInt32BigEndian()

		chunkCount := uint32(math.Ceil(float64(length) / float64(chunkSize)))
		chunksFound := make([]bool, chunkCount)

		for x := uint32(0); x < chunkCount; x++ {
			// Read the chunk number, they are not necessarily in order
			chunkNumber, _ := file.ReadUInt32BigEndian()

			if chunkNumber >= chunkCount {
				Panic("Chunk number read from stream is out of bounds: %d %d. File may be corrupted", chunkNumber, chunkCount)
			}

			if chunksFound[chunkNumber] {
				Panic("Chunk number %d was already encountered. File may be corrupted", chunkNumber)
			}

			// Read the size of the data from memory
			// This is not necessary the same size as chunkSize if the data is compressed
			dataSize, _ := file.ReadUInt32BigEndian()

			// Calculate the chunk offset
			chunkOffset := chunkNumber * chunkSize

			// Mark this chunk as found
			chunksFound[chunkNumber] = true

			Min := func(a, b uint32) uint32 {
				if a < b {
					return a
				}
				return b
			}

			// The chunk size is a maximum value and should be limited for the last chunk where it might not be
			// exactly equal to the chunk size
			actualChunkSize := Min(chunkSize, length-chunkOffset)

			// Read the chunk data
			rawData, _ := file.ReadBytes(int(dataSize))

			Decompress := func(byteArray []byte) []byte {
				reader := bytes.NewReader(byteArray)
				gzip, _ := gzip.NewReader(reader)
				result, _ := ioutil.ReadAll(gzip)
				return result
			}

			if formatVersion == 0 {
				decompressedData := Decompress(rawData)
				copy(data[chunkOffset:chunkOffset+actualChunkSize], decompressedData)
			} else {
				copy(data[chunkOffset:chunkOffset+actualChunkSize], rawData)
			}

		}

		// With the data read in as one large 1D array, we now format the data properly
		// Calculate the bits per pixel
		bpp := stride * 8 / int32(layeredImage.Width)

		// Convert 1D array to image array
		if bpp == 32 {
			var rgba image.Image = &image.RGBA{Pix: data,
				Stride: int(4 * layeredImage.Width),
				Rect: image.Rectangle{Min: image.Point{0, 0},
					Max: image.Point{X: int(width), Y: int(height)}}}
			layeredImage.Layers[layerName] = rgba
		} else {
			Panic("Invalid bpp %d", bpp)
		}
	}

	return &layeredImage
}
